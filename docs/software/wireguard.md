# Wireguard

[Wireguard](https://hub.docker.com/r/linuxserver/wireguard) WireGuard® is an extremely simple yet fast and modern VPN that utilizes state-of-the-art cryptography

**Supported Architectures:** amd64

## Setup

To enable Wireguard, run the following command:

**`hlos enable wireguard`**

To finalise any changes made, please run:

**`hlos update_one wireguard`**

More information about available commands can be found in the documentation, [here]()

## First run
### Admin setup

navigate to

## Access

Wireguard is available at [https://{% if wireguard.domain %}{{ wireguard.domain }}{% else %}{{ wireguard.subdomain + "." + domain }}{% endif %}/](https://{% if wireguard.domain %}{{ wireguard.domain }}{% else %}{{ wireguard.subdomain + "." + domain }}{% endif %}/) or [http://{% if wireguard.domain %}{{ wireguard.domain }}{% else %}{{ wireguard.subdomain + "." + domain }}{% endif %}/](http://{% if wireguard.domain %}{{ wireguard.domain }}{% else %}{{ wireguard.subdomain + "." + domain }}{% endif %}/)

{% if enable_tor %}
It is also available via Tor at [http://{{ wireguard.subdomain + "." + tor_domain }}/](http://{{ wireguard.subdomain + "." + tor_domain }}/)
{% endif %}

## Security enable/disable https_only and auth

To enable https_only or auth, run the corresponding example of either following command:

**`hlos https only`**
**`hlos auth enable`**

make either of the following changes to the `settings/config.yml` file:

```
wireguard:
  https_only: True
  auth: True
```
