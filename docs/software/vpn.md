# VPN

[VPN](https://hub.docker.com/r/arandomdocker/vpn-gateway) vpn-gateway

**Supported Architectures:** amd64

## Setup

To enable vpn, run the following command:

**`hlos enable vpn`**

To finalise any changes made, please run:

**`hlos update_one vpn`**

More information about available commands can be found in the documentation, [here]()

## First run

### Admin setup

## Access

vpn is available at [https://{% if vpn.domain %}{{ vpn.domain }}{% else %}{{ vpn.subdomain + "." + domain }}{% endif %}/](https://{% if vpn.domain %}{{ vpn.domain }}{% else %}{{ vpn.subdomain + "." + domain }}{% endif %}/) or [http://{% if vpn.domain %}{{ vpn.domain }}{% else %}{{ vpn.subdomain + "." + domain }}{% endif %}/](http://{% if vpn.domain %}{{ vpn.domain }}{% else %}{{ vpn.subdomain + "." + domain }}{% endif %}/)

{% if enable_tor %}
It is also available via Tor at [http://{{ vpn.subdomain + "." + tor_domain }}/](http://{{ vpn.subdomain + "." + tor_domain }}/)
{% endif %}

## Security enable/disable https_only and auth

To enable https_only or auth, run the corresponding example of either following command:

**`hlos https only`**
**`hlos auth enable`**

make either of the following changes to the `settings/config.yml` file:

```
vpn:
  https_only: True
  auth: True
```
