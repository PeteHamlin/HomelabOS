# Readarr

[Readarr](https://readarr.com/) Readarr is a ebook (and maybe eventually magazine/audiobook) collection manager for Usenet and BitTorrent users.

**Supported Architectures:** amd64

## Setup

To enable Readarr, run the following command:

**`hlos enable readarr`**

To finalise any changes made, please run:

**`hlos update_one readarr`**

More information about available commands can be found in the documentation, [here]()

## Access

Readarr is available at [https://{% if readarr.domain %}{{ readarr.domain }}{% else %}{{ readarr.subdomain + "." + domain }}{% endif %}/](https://{% if readarr.domain %}{{ readarr.domain }}{% else %}{{ readarr.subdomain + "." + domain }}{% endif %}/) or [http://{% if readarr.domain %}{{ readarr.domain }}{% else %}{{ readarr.subdomain + "." + domain }}{% endif %}/](http://{% if readarr.domain %}{{ readarr.domain }}{% else %}{{ readarr.subdomain + "." + domain }}{% endif %}/)

{% if enable_tor %}
It is also available via Tor at [http://{{ readarr.subdomain + "." + tor_domain }}/](http://{{ readarr.subdomain + "." + tor_domain }}/)
{% endif %}

## Security enable/disable https_only and auth

To enable https_only or auth, run the corresponding example of either following command:

**`hlos https only`**
**`hlos auth enable`**

make either of the following changes to the `settings/config.yml` file:

```
readarr:
  https_only: True
  auth: True
```
