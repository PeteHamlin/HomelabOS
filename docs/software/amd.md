# AMD

[AMD](https://hub.docker.com/r/randomninjaatk/amd) RandomNinjaAtk/amd is a Lidarr companion script to automatically download music for Lidarr

**Supported Architectures:** amd64

## Setup

To enable AMD, run the following command:

**`hlos enable amd`**

To finalise any changes made, please run:

**`hlos update_one amd`**

More information about available commands can be found in the documentation, [here]()

## First run
### Admin setup

navigate to

## Access

AMD is available at [https://{% if amd.domain %}{{ amd.domain }}{% else %}{{ amd.subdomain + "." + domain }}{% endif %}/](https://{% if amd.domain %}{{ amd.domain }}{% else %}{{ amd.subdomain + "." + domain }}{% endif %}/) or [http://{% if amd.domain %}{{ amd.domain }}{% else %}{{ amd.subdomain + "." + domain }}{% endif %}/](http://{% if amd.domain %}{{ amd.domain }}{% else %}{{ amd.subdomain + "." + domain }}{% endif %}/)

{% if enable_tor %}
It is also available via Tor at [http://{{ amd.subdomain + "." + tor_domain }}/](http://{{ amd.subdomain + "." + tor_domain }}/)
{% endif %}

## Security enable/disable https_only and auth

To enable https_only or auth, run the corresponding example of either following command:

**`hlos https only`**
**`hlos auth enable`**

make either of the following changes to the `settings/config.yml` file:

```
amd:
  https_only: True
  auth: True
```
