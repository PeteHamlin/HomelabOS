# Deemix

[Deemix](https://gitlab.com/Bockiii/deemix-docker)

**Supported Architectures:** amd64

## Setup

To enable Deemix, run the following command:

**`hlos enable deemix`**

To finalise any changes made, please run:

**`hlos update_one deemix`**

More information about available commands can be found in the documentation, [here]()

## First run
### Admin setup

navigate to

## Access

Deemix is available at [https://{% if deemix.domain %}{{ deemix.domain }}{% else %}{{ deemix.subdomain + "." + domain }}{% endif %}/](https://{% if deemix.domain %}{{ deemix.domain }}{% else %}{{ deemix.subdomain + "." + domain }}{% endif %}/) or [http://{% if deemix.domain %}{{ deemix.domain }}{% else %}{{ deemix.subdomain + "." + domain }}{% endif %}/](http://{% if deemix.domain %}{{ deemix.domain }}{% else %}{{ deemix.subdomain + "." + domain }}{% endif %}/)

{% if enable_tor %}
It is also available via Tor at [http://{{ deemix.subdomain + "." + tor_domain }}/](http://{{ deemix.subdomain + "." + tor_domain }}/)
{% endif %}

## Security enable/disable https_only and auth

To enable https_only or auth, run the corresponding example of either following command:

**`hlos https only`**
**`hlos auth enable`**

make either of the following changes to the `settings/config.yml` file:

```
deemix:
  https_only: True
  auth: True
```
