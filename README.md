This is my own personal fork of [HomelabOS](https://gitlab.com/NickBusey/HomelabOS) by Nick Busey. The base project is exceptional and I strongly advise that you go check them out first, as it has a larger development team behind it, and includes many more services by default. This is a less 'all batteries included' approach more tailored to my personal tastes.

## What's different

* Switched to Ansible 3.0, run locally
* Added a selection of services I used
* Returned to Makefile usage
* Includes a VPN service which other services can hook into and route their traffic

## Rationale

* I wanted a cleaner repo, with less obscure dependencies everywhere
* It was hard to keep up with upstream changes as my personal branch gradually drifted away from the base config wise

## Documentation

The docs for this project can be viewed locally by running `make docs`

## Summary

A set of Ansible scripts to configure a Docker based server with all sorts of goodies. Following the unix philosophy we gather together many specific tools to build the exact end result desired.

## Features

- Automated Backups
- Easy Restore
- Automated Tor Onion Service access
- Automated HTTPS via LetsEncrypt
- [Automated Settings Sync](https://homelabos.com/docs/setup/installation/#syncing-settings-via-git)
- Optional Cloud Bastion Server with WireGuard VPN

## Alternatives

- [HomelabOS](https://gitlab.com/NickBusey/HomelabOS)
- [Ansible NAS](https://github.com/davestephens/ansible-nas)
- [DockSTARTer](https://dockstarter.com/)

