.PHONY: decrypt encrypt deploy restore develop lint docs count_services

VERSION := $(cat VERSION)
PASSWORD = --vault-password-file=.homelabos_vault_pass
EXTRA_VARS= --extra-vars="@settings/config.yml" --extra-vars="@settings/vault.yml"

define notice
			@tput bold
			@tput setaf 6
      @echo "========== $1 =========="
			@tput sgr0
endef

# Deploy HomelabOS - `make`
deploy: git_sync config
	$(call notice,Deploying HomelabOS)
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -i inventory playbook.homelabos.yml

bastion:
	$(call notice,Deploying bastion)
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -i inventory playbook.homelabos_wireguard.yml

# Initial configuration
config:
# If config.yml does not exist, populate it with a 'blank'
# yml file so the first attempt at parsing it succeeds
	$(call notice,Updating configuration files)
	@mkdir -p settings/passwords
	@[ -f ./.homelabos_vault_pass ] || ./scripts/generate_ansible_pass.sh
	@[ -f settings/vault.yml ] || cp templates/config.yml.blank settings/vault.yml
	@[ -f settings/config.yml ] || cp templates/config.yml.blank settings/config.yml
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) $(PASSWORD) -i config_inventory playbook.config.yml
	$(call notice,Encrypting secrets)
	@ansible-vault encrypt ${PASSWORD} settings/vault.yml || true
	$(call notice,Done with configuration)

# Attempt to sync user settings to a git repo
git_sync:
	@./scripts/git_sync.sh || true

# Reset all local settings
reset_config:
	$(call notice,Reset local settings)
	@printf "\n - Backing up old settings.\n"
	mv settings settings.bak
	mkdir settings
	@printf "\n - Then we'll set up a blank config file.\n"
	cp config.yml.blank settings/config.yml
	$(call notice,Configuration reset! Now just run 'make config')

# Update just HomelabOS Services (skipping slower initial setup steps)
update: git_sync config
	$(call notice,Update HomelabOS)
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -i inventory -t deploy playbook.homelabos.yml
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -i inventory playbook.restart.yml
	$(call notice,Update completed!)

# Update just one HomelabOS service `make update_one inventario`
update_one: git_sync config
	$(call notice,Update $(filter-out $@,$(MAKECMDGOALS)))
	@ansible-playbook -e '{"services":["$(filter-out $@,$(MAKECMDGOALS))"]}' $(EXTRA_VARS) $(PASSWORD) -i inventory -t deploy playbook.homelabos.yml
	$(call notice,Restart $(filter-out $@,$(MAKECMDGOALS)))
	@ansible-playbook -e '{"services":["$(filter-out $@,$(MAKECMDGOALS))"]}' $(EXTRA_VARS) $(PASSWORD) -i inventory playbook.restart.yml
	$(call notice,Update completed!)

# Remove HomelabOS services
uninstall:
	$(call notice,Uninstall HomelabOS completely)
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -i inventory -t deploy playbook.remove.yml
	$(call notice,Uninstall completed!)

# Remove one service
remove_one: git_sync config
	$(call notice,Remove data for $(filter-out $@,$(MAKECMDGOALS)))
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -e '{"services":["$(filter-out $@,$(MAKECMDGOALS))"]}' -i inventory playbook.remove.yml
	$(call notice,Done removing $(filter-out $@,$(MAKECMDGOALS))!)

# Reset a service's data files
reset_one: git_sync config
	$(call notice,Removing data for $(filter-out $@,$(MAKECMDGOALS)))
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -e '{"services":["$(filter-out $@,$(MAKECMDGOALS))"]}' -i inventory playbook.stop.yml
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -e '{"services":["$(filter-out $@,$(MAKECMDGOALS))"]}' -i inventory playbook.remove.yml
	$(call notice,Redeploying $(filter-out $@,$(MAKECMDGOALS)))
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -e '{"services":["$(filter-out $@,$(MAKECMDGOALS))"]}' -i inventory -t deploy playbook.homelabos.yml
	$(call notice,Done resetting $(filter-out $@,$(MAKECMDGOALS))!)

# Run just items tagged with a specific tag `make tag tinc`
tag: git_sync config
	$(call notice,Running tasks tagged with '$(filter-out $@,$(MAKECMDGOALS))')
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -i inventory -t $(filter-out $@,$(MAKECMDGOALS)) playbook.homelabos.yml
	$(call notice,Done running tasks tagged with '$(filter-out $@,$(MAKECMDGOALS))'!)

# Restore a server with the most recent backup. Assuming Backups were running.
restore: git_sync config
	$(call notice,Restoring from backup)
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -i inventory restore.yml
	$(call notice,Done restoring from backup!)

# Run linting scripts
lint:
	$(call notice,Running Linter)
	@pre-commit run --all-files

# Restart all enabled services
restart: git_sync config
	$(call notice,Restarting all services)
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -i inventory playbook.restart.yml
	$(call notice,Done restarting all services!)

# Restart one service
restart_one: git_sync config
	$(call notice,Restarting '$(filter-out $@,$(MAKECMDGOALS))')
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -e '{"services":["$(filter-out $@,$(MAKECMDGOALS))"]}' -i inventory playbook.restart.yml
	$(call notice,Done restarting '$(filter-out $@,$(MAKECMDGOALS))'!)

# Stop all enabled services
stop: git_sync config
	$(call notice,Stopping all services)
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -i inventory playbook.stop.yml
	$(call notice,Done stopping all services!)

# Stop one service
stop_one: git_sync config
	$(call notice,Stopping '$(filter-out $@,$(MAKECMDGOALS))')
	@ansible-playbook $(EXTRA_VARS) $(PASSWORD) -e '{"services":["$(filter-out $@,$(MAKECMDGOALS))"]}' -i inventory playbook.stop.yml
	$(call notice,Done stopping '$(filter-out $@,$(MAKECMDGOALS))'!)

# Spin up cloud servers with Terraform https://homelabos.com/docs/setup/terraform/
terraform: git_sync
	$(call notice,Deploying cloud server!)
	@[ -f settings/config.yml ] || cp config.yml.blank settings/config.yml
	@./scripts/terraform.sh
	$(call notice,Done deploying cloud servers! Run 'make')

# Destroy servers created by Terraform
terraform_destroy: git_sync
	$(call notice,Destroying cloud services!)
	@/bin/bash -c "cd settings; terraform destroy"
	$(call notice,Done destroying cloud services!)

decrypt:
	$(call notice,Decrypting Ansible Vault!)
	@ansible-vault decrypt ${PASSWORD} settings/vault.yml
	$(call notice,Vault decrypted! settings/vault.yml)

encrypt:
	$(call notice,Encrypting Ansible Vault!)
	@ansible-vault encrypt ${PASSWORD} settings/vault.yml
	$(call notice,Vault encrypted! settings/vault.yml)

set:
	$(call notice,Setting '$(filter-out $@,$(MAKECMDGOALS))')
	@./scripts/set_setting.sh $(filter-out $@,$(MAKECMDGOALS))
	$(call notice,Done!)

get:
	$(call notice,Getting '$(filter-out $@,$(MAKECMDGOALS))')
	@./scripts/get_setting.sh $(filter-out $@,$(MAKECMDGOALS))
	$(call notice,Done!)

# Spin up a development stack
develop: config
	$(call notice,Spinning up dev stack)
	@[ -f settings/test_config.yml ] || cp settings/config.yml settings/test_config.yml
	@vagrant up --provision
	$(call notice,Done spinning up dev stack!)

# Serve the HomelabOS Documentation locally
docs:
	@docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material

# Return the amount of services included in this version of HomelabOS
count_services:
# This lists each folder in roles/ on it's own line, then excludes anything with homelabos or 'docs' in it, which are HomelabOS things and not services. Then it counts the number of lines.
	@ls -l roles | grep -v --extra-vars="homelab" --extra-vars="docs" | wc -l

# Hacky fix to allow make to accept multiple arguments
%:
	@:
